package es.fpdual.store.product.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import es.fpdual.store.product.entity.Category;
import es.fpdual.store.product.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

    public List<Product> findByCategory(Category category);

    public List<Product> findByUserId(Long userId);

}
