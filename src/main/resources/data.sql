delete from Categories;

INSERT INTO Categories (id, name) VALUES (1, 'zapatos');
INSERT INTO Categories (id, name) VALUES (2, 'libros');
INSERT INTO Categories (id, name) VALUES (3, 'electronica');

delete from Products;

INSERT INTO Products (id, name, description, stock, price, status, created_at, category_id, user_id)
VALUES (1, 'Nike Airforce 1','La leyenda sigue viva en las Air Force 1 ''07 para hombre, una versión modernizada de las icónicas AF1 que mezcla un estilo clásico con detalles innovadores.',5,109.99,'CREATED','2018-09-05', 1, 1);

INSERT INTO Products (id, name, description, stock, price, status, created_at, category_id, user_id)
VALUES (2, 'under armour Men ''s Micro G Assert – 7','Parte superior de malla ligera que ofrece transpirabilidad completa. Revestimientos de cuero duraderos para mayor estabilidad',4,12.5,'CREATED','2018-09-05', 1, 1);

INSERT INTO Products (id, name, description, stock, price, status, created_at, category_id, user_id)
VALUES (3, 'Luna de Plutón','Está siendo un éxito en todos los países de habla hispana',12,40.06,'CREATED','2018-09-05', 2, 2);