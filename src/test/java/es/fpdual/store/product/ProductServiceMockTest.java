package es.fpdual.store.product;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import es.fpdual.store.product.entity.Category;
import es.fpdual.store.product.entity.Product;
import es.fpdual.store.product.repository.ProductRepository;
import es.fpdual.store.product.service.ProductService;
import es.fpdual.store.product.service.ProductServiceImpl;

@SpringBootTest
public class ProductServiceMockTest {

    @Mock
    private ProductRepository productRepository;

    private ProductService productService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        productService = new ProductServiceImpl(productRepository);

        Product product01 = Product.builder()
                .name("El libro troll")
                .category(Category.builder().id(2L).build())
                .description("Un buen libro troll")
                .stock(Double.parseDouble("20"))
                .price(Double.parseDouble("19.99"))
                .build();

        Mockito.when(productRepository.findById(1L))
                .thenReturn(Optional.of(product01));

        Mockito.when(productRepository.save(product01))
                .thenReturn(product01);
    }

    @Test
    public void whenValidGetID_expectProduct() {
        Product found = productService.getProduct(1L);
        Assertions.assertThat(found.getName()).isEqualTo("El libro troll");
    }

    @Test
    public void whenValidUpdateStock_expectNewStock() {
        Product newStock = productService.updateStock(1L, Double.parseDouble("8"));
        Assertions.assertThat(newStock.getStock()).isEqualTo(28);
    }
}
